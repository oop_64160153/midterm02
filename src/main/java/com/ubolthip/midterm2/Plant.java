package com.ubolthip.midterm2;

public class Plant {
    private String name;
    private char symbol;
    private int x;
    private int y;
    private boolean die;

    // Constructor พืช
    public Plant(String name, char symbol, int x, int y) {
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
        this.die = false;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }

    public String getName() {
        return name;
    }

    // ถ้า return false ซอมบี้จะตาย return true ยังไม่ตาย
    public boolean shot(Zombie z) {
        if (z.getDie()) {
            return false;
        }
        // random critical 0-99 โอกาสคิด Critical 10%
        int critical = (int) (Math.random() * 100);
        int damage = 1;
        if (critical >= 90) {
            damage = 2;
        }
        System.out.println(this.symbol + " shot " + z.getSymbol() + " damage " + damage);

        // ค่า HP ปัจจุบัน
        int nowHp = z.getHp() - damage;
        if (nowHp <= 0) {
            System.out.println(z.getSymbol() + " is Die ");
            return false;
        }
        // set ค่า HP ใหม่หลังจากที่โดนพืชตี
        z.setHp(nowHp);
        System.out.println("Now " + z.getSymbol() + " HP is " + nowHp);
        System.out.println();
        return true;
    }

    public void setDie(boolean die) {
        this.die = die;
    }

    public boolean getDie() {
        return die;
    }
}
