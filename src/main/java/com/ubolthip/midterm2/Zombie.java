package com.ubolthip.midterm2;

public class Zombie {
    private String name;
    private char symbol;
    private int x;
    private int y;
    private int hp;
    private boolean die;

    public Zombie(String name, char symbol, int x, int y) {
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
        // HP(5-12) = max-min+1 (12-5+1)
        this.hp = (int) (Math.random() * 8) + 5;
        this.die = false;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }

    // ถ้าตำแหน่ง X ของซอมบี้ = 0 คือเดินถึงพืช จะ return false
    public boolean walk() {
        if (x == 0) {
            return false;
        }
        x = x - 1;
        return true;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void printHP() {
        System.out.println(name + " hp is " + hp);
    }

    public boolean getDie() {
        return die;
    }

    public void setDie(boolean die) {
        this.die = die;
    }
}
