package com.ubolthip.midterm2;

public class App {
    public static void main(String[] args) {
        final int X_MIN = 0;
        final int X_MAX = 8;
        final int Y_MIN = 0;
        final int Y_MAX = 2;

        Plant P = new Plant("Peashooter", 'P', X_MIN, 0);
        Plant R = new Plant("Repeater", 'R', X_MIN, 1);
        Plant S = new Plant("SnowPea", 'S', X_MIN, 2);

        Zombie B = new Zombie("BucketheadZombie", 'B', X_MAX, 0);
        Zombie C = new Zombie("ConeheadZombie", 'C', X_MAX, 1);
        Zombie Z = new Zombie("Zombie", 'Z', X_MAX, 2);

        System.out.println("!!!Game Start!!!");
        System.out.println();
        B.printHP();
        C.printHP();
        Z.printHP();

        System.out.println();
        while (true) {
            for (int y = Y_MIN; y <= Y_MAX; y++) {
                for (int x = X_MIN; x <= X_MAX; x++) {
                    // zombie ต้องไม่ตายถึงจะแสดง symbol บน map 
                    if (B.getX() == x && B.getY() == y && !B.getDie()) {
                        System.out.print(B.getSymbol());
                    } else if (C.getX() == x && C.getY() == y && !B.getDie()) {
                        System.out.print(C.getSymbol());
                    } else if (Z.getX() == x && Z.getY() == y && !Z.getDie()) {
                        System.out.print(Z.getSymbol());
                    } else if (P.getX() == x && P.getY() == y) {
                        System.out.print(P.getSymbol());
                    } else if (R.getX() == x && R.getY() == y) {
                        System.out.print(R.getSymbol());
                    } else if (S.getX() == x && S.getY() == y) {
                        System.out.print(S.getSymbol());
                    } else {
                        System.out.print('-');
                    }
                }
                System.out.println();
            }

            System.out.println();

            // plant ยิง zombie
            B.setDie(!P.shot(B));
            C.setDie(!R.shot(C));
            Z.setDie(!S.shot(Z));

            // ถ้า zombie ทุกตัวตายจะชนะ
            if (B.getDie() && C.getDie() && Z.getDie()) {
                print();
                System.out.println("You Win");
                break;
            }
           
            System.out.println();

            // zombie เดินไปทางซ้าย 1 ช่อง
            P.setDie(!B.walk());
            R.setDie(!C.walk());
            S.setDie(!Z.walk());
            // ถ้า plant ตัวใดตัวหนึ่งตายจะแพ้
            if (P.getDie() || R.getDie() || S.getDie()) {
                print();
                System.out.println("You Lose");
                break;
            }
            print();
        }
    }

    public static void print() {
        for (int i = 0; i < 15; i++) {
            System.out.print('+');
        }
        System.out.println();
    }
}
